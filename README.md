

# Animated moving circle for EMDR

So far implemented only using CSS3.

## Backlog

* smooth animation (https://medium.com/outsystems-experts/how-to-achieve-60-fps-animations-with-css3-db7b98610108)
* set speed
* set size
* set color
* set timer for length of session
* some loop settings or repeated sessions with set time


